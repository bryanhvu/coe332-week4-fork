# File IO for the Project

Dealing with files will be useful in three places

  * Generating files from plots using matplotlib.
  * Saving files to the Redis database.
  * Returning files from the Flask API.

We'll cover each, one by one.

## Generating Files from Plots Using Matplotlib

Generally speaking, matplotlib makes it relatively easy to generate a file from a plot object. The simplest approach
is to use the `savefig` function on the plot object.

  * The `savefig()` function takes one required argument: a path on the local file system where the file should be
    saved.
  * The `dpi` option should usually also be given to specify a resolution ("dots per inch") to use for the file. A
    value of `150` for files like we are going to be dealing with is fine.

Here is a complete example:

```
import matplotlib.pyplot as plt

# two lists of data corresponding to the x and y-axis, respectively:
t = [1,2,3,4,5]
d = [3,6,7.5, 9, 14]

# create the scatter plot
plt.scatter(t, d)

# save to a file
plt.savefig('/tmp/myfile.png', dpi=150)
```

## Saving Files to the Redis Database

As an in-memory data store (the entire dataset has to fit into main memory) Redis is not a great choice for
directly storing large collections of big files. But for smaller files like our plots, one approach to saving files
directly in Redis is simply to store the raw bytes of the file as the value of a key.

Keep in mind:

  * The maximum size for a key of value is typically 512 MB.
  * The sum of all data (including files) must fit into main memory on the server running Redis, along with any
    other programs running.

With that said, saving a file amounts to reading in the data and storing it in a field in Redis. Here is an example:

```
import redis
rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=0)

# read the raw file bytes into a python object
file_bytes = open('/tmp/myfile.png', 'rb').read()

# set the file bytes as a key in Redis
rd.set('key', file_bytes)
```

We could also have used other Redis commands such as `rd.hmset()`, etc., to store the files bytes into other structures.

NOTE: When storing using `hmset('<job_id>', {'id': '<job_id>', ..., 'plot': file_bytes})` retrieval using
```
plot = hmget('<job_id>', 'plot')
```

will return a *list of bytes* (of length 1), not a pure bytes object.

To get an actual bytes object, one needs to use `plot[0]`.


## Serving Files in Flask

Ultimately, we want our users to be able to retrieve the files we generate using an endpoint in our API.

  * It will be easier to have a separate endpoint for doing this task, something like `/jobs/{jid}/plot`.
  * To simplify things, the response to this endpoint just be the file, as binary data, not any JSON data.
  * If there is some error, we can indicate that with the response code.
  * We will use flask's `send_file` function whose primary argument is a
  * We will also use the `as_attachment=True` feature of `send_file` so that `Content-Disposition: attachment` is sent
    as a response header. Among other things, this tells http clients to not try and display the contents "inline".

A word on file-like objects and byte-streams.

  * Streams (aka "file-like objects") are a generalization of the normal file concept and come equipped with
    capabilities similar to files; e.g., `read()` and `write()` and also modes, e.g. `read-only`, `read-write`.
  * Python divides streams into text streams, bytes streams (binary) and raw streams.
    * TextIO streams deal with string objects; i.e., objects of type `str`.
    * BytesIO streams deal with `byte` objects
    * RawIO streams provide low-level access to the storage device, typically through an Operating System API.
  * For us, BytesIO streams will do the job.

To use create stream objects, use the `io` library.

```
import io

# create a bytes stream object out of some raw bytes:
bstream = io.BytesIO(some_bytes)

# do something with the stream
bstream.write(b'abc...')

```

Now that we know how to create byte streams, it is straight-forward to implement `send_file` in our API.

Here is a more or less complete example:

```
from flask import send_file

# inside your flask function:
@app.route('/jobs/<job_id>/plot', methods=['GET']):
def get_job_plot(job_id):

    # do something to get the plot from the database:
    plot = get_job_plot_from_db(job_id)
    return send_file(io.BytesIO(plot),
                     mimetype='image/png',
                     as_attachment=True,
                     attachment_filename='{}.png'.format(job_id))
```
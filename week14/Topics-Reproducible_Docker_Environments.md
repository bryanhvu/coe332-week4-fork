# Reproducible Software Environments Via Docker

One of the major points behind using Docker is to simplify maintenance and increase reproducibility of software
environments. The idea is this:

  * Each piece of software is packaged into its own, self-contained, image.
  * To create a new environment or server forming part of an environment, all that is required is:
    * The Docker runtime.
    * The software image or images that will be run on that server.
    * A script (such as a docker-compose file) to start the necessary containers from the images.
  * The images can be stored and downloaded from DockerHub to simplify distribution.

We should be able to recreate our Flask API system on a new set of VMs with relative ease. The steps would be:

  * Install Docker and docker-compose.
  * Download the compose file(s) needed (or check them out of bitbucket).
  * Start the containers with `docker-compose up`.

Note that the step of downloading the images happens automatically when we `docker-compose up`.


## Working with Docker Images

Docker images are stored locally on each Docker machine to speed up starting containers but they take up space.

You can list the images on a given machine with
```
$ docker images
```

and you can delete an image with
```
$ docker rmi <image_id>
```

To remove all docker images:
```
$ docker rmi `docker images -q`
```
This will likely require you to remove all containers first, which can be done with:

```
$ docker rm -f `docker ps -aq`
```

We should be able to remove all docker images and still relatively easily reproduce our API environment by re-pulling
 the images.

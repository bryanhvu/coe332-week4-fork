# Introduction to Authentication in HTTP

All the requests we have made in this class to date have been *anonymous* requests - we hadn't provided any identity
information to prove who was making the requests. In the real world, APIs will require a client to prove
who it is and that it is authorized to make specific requests (most commonly, requests that make changes to data,
i.e., POST, PUT and DELETE methods, but also potentially GET requests that request private data).

> We will be illustrating some of the concepts using the github API. To follow along use your github account.


## Authentication
Authentication is the process of proving to a third-party (in our case, an API) you are who you say you are. At the
DMV, this amounts to producing your driver's license. There are multiple ways of authenticating to APIs. We'll look at
two of them.


### HTTP Basic Auth
HTTP basic auth uses two pieces of information, a public identifier (such as a username) and a secret (such as a
password). To authenticate to an API using HTTP Basic Auth, we create an `Authorization` header.

> Recall that each request and response has a set of attribute-value pairs called *headers* that describe metadata
about the message. Common headers include `Content-type` (with values such as `application/json` or `text/html`
describing the type of content in the message) `Cookie` (used to store session data), and `Authorization`
(for authentication).

To use HTTP Basic Auth, we:

  * Create a string combining the identifier and the secret with a colon and
  * base64 encoding the entire result.
  * In notation, `Base64(<identified>:<secret>)`.

Base64 encoding is a scheme for converting binary data to text (ASCII). It is easily reversible so it does not provide
additional security, but it does provide a way for converting usernames and passwords with "special characters" into
an HTTP-compliant header.

#### Using HTTP Basic Auth

Since implementing Basic Auth amounts to setting a header, we can always set the header directly in our http client.
For example:

  * In `curl`, we use `-H "Authorization: <value>"` to set the Authorization header.
  * In python using the `requests` library, we can set the `headers={}` attribute to the `requests` methods;
    e.g. `requests.get(..., headers={'Authorization': '<value>'})`

Note that both of these methods require us to base64 encode the `<intentifier>:<secret>` string.

Both `curl` and the `requests` library also provide convenience methods for setting the authorization header.

  * With `curl`, set the `-u` flag; we can either put `curl -u <identifier>:<secret> ...` or simply `curl -u <identifier> ...`;
    In the latter case, `curl` will prompt us for the secret.
  * In python `requests`, we can set the `auth` attribute in the `requests` methods; e.g. `requests.get(.... auth=(identifier, secret))`

### HTTP Basic Auth in the GitHub API

Let's try this out using the GitHub API. Note the following facts about the GitHub API:

  * The base URL for the GitHub API is `api.github.com`.
  * The API returns JSON data in all of its (non-binary) responses.
  * The `/user` endpoint will return information about the authenticated user.
  * All (authenticated) API requests must go over HTTPS for security purposes.


```
Exercise 1. Make an authenticated request to the github API using HTTP Basic Auth with your username and password. Use
the `https://api.github.com/user` to pull information about the authenticated user. What type of plan do they have? How many
private repos are they allowed?
```

Note that we didn't provide a username in our request to the GitHub API; instead, the user in question was derived from
the Authorization header.

Any action we want to take on a specific user's account involving creating, updating or deleting objects inside GitHub
is going to require authentication as well.

  * We can create a new repository for the authenticated user using the `/user/repos` endpoint. What method do you expect
    it to require?
  * We also need to pass a JSON object describing the repository we want created. A full list of required and optional
    fields is available here: https://developer.github.com/v3/repos/#create


```
Exercise 2. Let's create a new github repository by making a POST request using Basic Auth to the
`https://api.github.com/user/repos` URL. Make sure to git the repository a description.

Exercise 3. Retrieve a complete description of the repository you just created using an authenticated GET request to
the repo's `url` (not it's html_url).
```

> Note: Python's `getpass` library provides a convenient way for securely collecting a password inside a Python script.
See usage example below:

```
import requests
from getpass import getpass
pd = getpass(prompt="Enter you github password: ")
user = "joestubbs5@gmail.com"
rsp = requests.get("https://api.github.com/user", auth=(user, pd))
```

Creating the repository:

```
repo = {'name': 'test-coe332', 'descrption':'test repo for COE 332'}
rsp = requests.post("https://api.github.com/user/repos", auth=(user, pd), json=repo)
rsp.json().get('url')
```

The github API uses the PATCH method for updating to an object, though it seems to require a full description of the
object -- all required fields.

```
Exercise 4. Let's use a PATCH request to turn off the "projects" feature of our test repo. Verify that your change took. Consult the github docs as needed: https://developer.github.com/v3/repos/#edit
```

Finally, since this was just an exercise, let's remove this test repository.

```
Exercise 5. Use a DELETE request to remove the test repository we created. API docs: https://developer.github.com/v3/repos/#delete-a-repository
```

> Note that it is typical for RESTful APIs to return an empty response to a DELETE request. What happens if you try to call the `json()` method on the github response to request in exercise 5? How can you validate that the request was succesful?

At this point the repository should be deleted.

Hopefully the power of HTTP APIs is starting to become clear. Say we needed to create 100 repositories? Or 1000? That would be a lot of clicks in the github website, but using Python, we could write a short program to create them in no time, assuming we knew the names of the repositories we needed to create. Moreover, we used Python because everyone knows it's the best programming language (jk), but nothing about what we did required Python. HTTP is a language-agnostic protocol: any programming language capable of sending messages over a socket can be used, and virtually every modern language has a high-level HTTP library such as `requests` for handling many of the minute details for us.

### OAuth

HTTP Basic Auth is pretty easy to use, but it has some limitations:

 * We have to pass the secret into every request.
 * Every application that wants to make API requests for a given user has to have the user's password.

What if we are writing an application for others and they do not want to share their github password with us?
This is where OAuth comes in.

If you ever used a site that said "Log in with your Facebook or Google account" then you have used OAuth.

#### OAuth Basic concepts

The key concepts in OAuth are:

  * Three parties are involved in OAuth: a provider, a client and a user.
    * An OAuth provider represents an authoritative source of identity information (essentially, any entity that
      maintains a collection of user accounts).
    * An OAuth client typically represents an *application* such as a web or mobile application, a command line script, etc.
    * Clients use the OAuth provider to interact with resources on behalf of the user.
  * OAuth provides different protocols called *grant types* to enable clients to use different processes to gain access
    to a user's resources.
  * A successful OAuth authentication results in the client obtaining a short term *access token* representing the user
    from the provider. The client can then use this access token to access resources until the token expires. It is
    typical for OAuth tokens to expire in a few hours, though there are mechanisms for "refreshing" them.

We're going to walk through some OAuth concepts using the TACC OAuth provider

To get started, we are going to generate a set of TACC OAuth client keys (OAuth client credentials). Generating OAuth
clients uses HTTP Basic Authentication with your TACC username and password.
  * The TACC APIs base URL is `https://api.tacc.utexas.edu`
  * The URL clients service is `https://api.tacc.utexas.edu/clients/v2`; the `v2` here indicates that we are using
    version 2 of the API platform (the current version). All core TACC API URLs are versioned.

```
Exercise. Use HTTP Basic Auth with your TACC username and password against the clients service to list all your OAuth
clients. Explore some of the endpoints within the clients service. Discuss the notion of API subscriptions.
```

#### Creating a Client
To create a simple OAuth client that has access to all basic APIs, we need to make a POST request to the clients
service. The only required field we need to pass in is `clientName` to give a name to our client. Each client we
create must have a unique name.

```
Exercise. Generate an OAuth client by making a POST request to the clients service.
```

Clients are identified by their key and secret. These two important properties are returned in the fields
`consumerKey` and `consumerSecret` respectively.

```
Exercise. Extract the key and secret from the client you just generated into variables in the Python shell. We will
need these fields to interact with the OAuth token service and generate an access token.
```

### Generating Access and Refresh Tokens

We're now ready to generate an OAuth token. This token will represent both the user and client application. In this
case, they are owned by the same individual, but in general they will not be.

To generate an OAuth token, we make a POST request to TACC's token service. We have to pass in several fields to make
the request:

  * `username` - the username of the end user represented by the token
  * `password` - the password of the end user represented by the token
  * `grant_type` - the OAuth grant type we are using (in this case `password`).
  * `scope` - the OAuth scope we want. In this case, simply use `PRODUCTION`.

> It might seem odd that we are passing in the username and password when that was one of the things we were trying
to avoid. There are two points to make:

  * first, we only have to pass it in once to get the access token and then all subsequent requests will use the
    access token.
  * second, in general we could use a different grant type to not have to collect the password at all, but in this
    introduction we are taking the simplest approach.

A note about scopes:
> scopes in OAuth can be used to limit/restrict the kinds of access the client has. The scope of `PRODUCTION` indicates
that the client will have full access (i.e., read, write, update, execute) to the user's resources.

Also, keep in mind that the TACC `token` service URL is simply `https://api.tacc.utexas.edu/token` - it does not
have a `v2` in it.

> Semantically, the idea was that the token service was independent of the version of the API platform version.

The response, if successful, will contain the following fields:

  * `access_token` - the access token representing the end user and client.
  * `refresh_token` - a token that can be used to retrieve a new access token using the `refresh_token` grant type.
  * `expires_in` - the time, in seconds, that the `access_token` will expire.
  * `token_type` - the token type (will have value `bearer` -- this is to comply with the OAuth spec).
  * `scope` - the scope associated with the token (will have value `default`)

```
Exercise. Generate an access and refresh token using the TACC token endpoint.
```

### Using an Access Token
Once we have an access token we are ready to interact with the rest of the API services. All requests to
the APIs using this access token will be done on behalf of the user whose credentials were used to retrieve the token
(as well as the OAuth client that was used).

In order to make a request to a TACC API using the access token, we need to pass the token into the Authorization
header of the request. The value of the header must be formatted like so: "Bearer <access_token>"

As a simple check, we'll use the Profiles service to pull the "profile" associated with this token. The Profiles API
maintains some details about registered users.
  * Base URL for the Profiles service: https://api.tacc.utexas.edu/profiles/v2
  * "me" endpoint - https://api.tacc.utexas.edu/profiles/v2/me ("me" is a special reserved word to indicate we want
    information about the associated token.)

```
Exercise. Use the access token to make a request to the Profiles service "me" endpoint to retrieve the profile
associated with the token.
```



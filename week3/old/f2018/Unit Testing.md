# Project

We wanted to provide a few details on the class project you will be working on. The overall goal is a cloud-based,
distributed system that provides the following capabilities:

* web-accessible API providing search and reporting capabilities for a time-series dataset in real-time.
* Asynchronous "jobs" API to enable custom analyses based on user-defined inputs.
* Use of architecture principles such as HTTP, REST, and JSON to enable access to the system from any (modern) programming language.
* Cloud-ready and environment-agnostic so that the system can be deployed virtually anywhere and scaled to meet user demand using an arbitrary number of additional resources.

In this directory you will find a diagram representing the architecture for the final project.

We will introduce all of these concepts in due time; for now, you will want to pick a time-series dataset for your first assignment.


# Testing, Part I (Unit testing)

Testing is an essential part of software engineering, especially for larger projects. As the system gets larger and/or as more people begin contributing to it, the importance of

This class will not be a comprehensive treatment of software testing; the goal is more to develop some initial familiarity with the basic concepts.


## Goals of Testing

The ultimate goal of testing is to verify some aspect of software quality; different kinds of tests are used for different kinds of quality measures. Some common ones are:

* Correctness
* Performance
* Installability/Compatibility with infrastructure and/or other systems
* Usability
* Security

We will focus on testing that is performed using a “test program”, a program that executes test cases against our code, and we will focus on correctness tests.


## Testing Types and Techniques
There are lots of different test types and techniques. In this class we will focus on two test types:

* Unit tests - each test exercises a narrow section (or unit) of code.
* Integration tests - tests exercise the integration of multiple components of a software application.

Unit tests:

  * Each test targets a single function.
  * Typically, there will be multiple unit tests for a single function to test different inputs and other code paths.
  * Ideally, each test should be independent of all others so that it can be run on its own.
  * Narrow scope of test means failure identifies responsible code
  * Requires a lot more tests to cover the entire application/system.

Integration tests:

  * Each test targets a higher-level capability, requirement or behavior of the system, and exercises multiple components of the system working together.
  * Broader scope means fewer tests are required to cover the entire application/system.
  * A given test failure provides more limited information as to the root cause.

## Testing in Practice
Here are some additional observations based on our experience with large software engineering projects:

  * Tests can provide a specification for how the code they exercise should work for other developers. They can provide a supplement to (but not a replacement for) other written documentation. 
  * Test suites allow multiple (even many) developers to work on a project without knowing how the entire system works. On really large software systems (for example, an Operating System), it is very common for no one person to understand the entire code base.
  * Tests can be used to make sure changes to add new features don't inadvertently break existing (working) features. This is called regression testing.
  * Most projects employ a mix of unit tests and integration tests. Mission-critical components or components that are especially complex tend to have more robust testing.


## Testing Frameworks
Testing frameworks are software libraries and/or systems that help our testing efforts by automating:

* Set up and tear down (tasks to do before and after every test).
* Collecting and running the tests
* Reporting out results.

In this class we will be using the py.test framework to write our test programs.

## Hands On ##
For the remaining sections, follow along on the class VM.

### Installation -
py.test not part of the Python standard library but can be installed using pip; this has already been done for you on the class VM.

Check access to the py.test framework by typing the following:

```shell
$ py.test-3
```

### Creating a Test File and Writing the First Test
Suppose we have the following function in a python module called primes.py:

```shell
def get_primes():
    """Returns the prime numbers between 2 and 100."""
    primes = []
    for i in range(2, 100):
        for j in range(2, i):
            if (i % j == 0):
                break
            if j == i-1:
                primes.append(i)
    return primes
```

To start writing tests for this function, create a new file in the same directory called test_primes.py. Add the following:

```
from primes import get_primes
```
The strategy it to write a set of functions that call the function(s) we want to test and make sure the result returned is what we expect. Therefore, we will need to import any functions we want to test.

Add a test function to our test file; we can use any name for the function that begins with `test_`. The rest of the name should indicate something about what the test is testing.

```
def test_primes_returns_list():
    assert type(get_primes()) == list
```

We use the `assert` statement to check aspects of the result, in this case that the type returned was a list.


### Running the tests
We can run all the tests in a test file using the following:
```
$ py.test-3 test_primes.py
```

The output should like similar to the following:
```
============================= test session starts ==============================
platform linux -- Python 3.4.9, pytest-2.8.5, py-1.4.30, pluggy-0.3.1
rootdir: /home/jstubbs, inifile:
collected 1 items

tests_v1.py .

=========================== 1 passed in 0.01 seconds ===========================
```


### Multiple Tests Example

We can add additional tests directly into the test_primes.py file simply by adding more functions. It is best practice to test only one aspect of a program with each test.

```
from primes import get_primes_v1

def test_primes_return_list():
    assert type(get_primes_v1()) == list

# this function won't be executed by the test runner because it does not begin with 'test_'
def foo():
   assert 1 == 0

# will this test pass?
def test_number_of_primes():
    assert len(get_primes_v1()) == 25
```

Example py.test report with failures:

```
================================== test session starts ==================================
platform linux -- Python 3.4.9, pytest-2.8.5, py-1.4.30, pluggy-0.3.1
rootdir: /home/jstubbs, inifile:
collected 2 items

tests_v1.py .F

======================================= FAILURES ========================================
_________________________________ test_number_of_primes _________________________________

    def test_number_of_primes():
>       assert len(get_primes_v1()) == 25
E       assert 24 == 25
E        +  where 24 = len([3, 5, 7, 11, 13, 17, ...])
E        +    where [3, 5, 7, 11, 13, 17, ...] = get_primes_v1()

tests_v1.py:10: AssertionError
========================== 1 failed, 1 passed in 0.02 seconds ===========================
```

Note that it the report shows us which tests passed and which failed. For tests that failed, it shows us  which assertion failed and what each expression in the assertion evaluated to at run time. In this case, `assert 24 == 25` caused the failure.

### Running Just One Test

As a test suite gets very large, it can take time to run from beginning to end. The py.test framework allows us to run just one test to save time. This is especially nice when we are trying to fix code to make a failing test start passing.

Use the following syntax to run just one test
```
$ py.test-3 <test_file>::<test_function>
```

For example,
```
$ py.test-3 test_primes.py::test_number_of_primes
```
will just run the `test_number_of_primes` test within the `test_primes.py` test file.

### Exercises.

1. Fix the `get_primes` function so that both tests pass; run the tests to validate your fix.
2. Modify the `get_primes` function to take an optional parameter, `max`, and have the function return all primes between 2 and `max`. The default value for `max` should be 100. Re-run the tests. Do they still all pass?
3. The number of primes between 2 and 1,000 is `168`. Add a test to check that the new version of your function works when called with `max=1000`.
4. Write a test to make sure every item returned from your function is an int type.


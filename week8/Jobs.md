# Adding Jobs to the Flask Application

We will be adding support for creating and retrieving "jobs" objects to our Flask application. Each "job" will represent
some analysis to do on a subset of our dataset. As we define the actual analyses jobs in more detail, we may need
to add more details to the jobs object.

## The Jobs Object

Since jobs data will eventually be accessed and processed from several places in our application (multiple endpoints in
the flask app, the workers, etc) we want to centralize the logic needed to read and write jobs objects in the Redis
database.


### Unique IDs for Each Job

Each job needs a unique ID so that clients, the Flask API, and (eventually) the workers have a mechanism for referring
to a specific job.

  * We will use a UUID (more on UUIDs below).
  * We will store the ID as part of the key used for the job object in the database.
  * To make application extensible and allow for additional object types, the full key format will be `job.<uuid>`

#### What is a UUID and How Do I Generate One?

UUIDs are:

  * 128 bit numbers generated using standards-based algorithms that are unique for all practical purposes (i.e., the
    probability of collisions is so low that, to get to a 50% probably of collision, one would need to generate 2.7x10^18 UUIDs).
  * There are 4 major versions of the standard (referred to as Version 1, ..., Version 4). Each has tradeoffs.
  * The algorithms can and have been implemented in most major programming languages (yay standards!) and can generate
    uuid's very quickly.
  * We will use UUID version 4 because: it generates uuid's with very low probability of collision without using sensitive
    data such as the MAC address of the server which generates the UUID.

To generate them in Python, we use the `uuid` package, included in the standard library.

```
>>> import uuid
>>> In [56]: uuid.uuid4()
Out[1]: UUID('56849963-e90d-4322-a369-50870f0cf9fa')

# return a string:
>>> str(uuid.uuid4())
Out[2]: '66a3acf3-8009-4cd3-8d40-c8ce42229f08'
```

### Status

Jobs will have a natural "life-cycle" that we will capture through a "status" field.

  * A job will start off in "SUBMITTED" status right after the client submits a request to create it.
  * Once a job has been picked up by a worker, it will enter "IN PROCESS" status.
  * After the worker completes the job, if all goes well the job will enter "COMPLETED" status.
  * Otherwise, the job status will change to "ERROR".

### User-Supplied Inputs

Since the set of data points is fixed, we'll allow for different analyses by enabling users to submit
"inputs" to the job. As a starting point:

  * Inputs will correspond to the parameters of the GET endpoints of the API; e.g. `start`, `end`, `limit`, `offset`, so
    that the analysis will be conducted on a subset of data points.
  * Additional inputs can be added over time to:
     * Allow options for a given type of analysis.
     * Allow different types of analysis jobs to be run. ("plot", "predict", etc.)

## Storing Jobs in Redis

To store the job data in Redis, we will create some new functions so that the code can be reused in multiple places.

### Define the Job Object

This is the kind of operation one might do in a class if one were using OOP. We can do it easily enough in a plain
function:

```
def instantiate_job(jid, status, start, end):
    """ Creates a Python object representing a job. """
    return {'id': jid,
            'status': status,
             'start': start,
             'end': end }
```

We are using a basic Python dictionary to hold our data: simple enough and gets the jobs done (pun intended).

We can also add functions to generate a job id and generate a Redis key for a job id:

```
import uuid

def generate_jid():
    return str(uuid.uuid4())

def generate_job_key(jid):
    return 'job.{}'.format(jid)
```

## Redis Data Type for Jobs

We have a choice here: we can either use a string with a fixed encoding to write JSON to Redis, or we can use a hash.
The choice is up to you; a Hash can be more flexible, as it will allow you to store different types of data, including
non-JSON serializable data (e.g., JPEG files).

We'll use a Hash in this example:

```
def add_job(jid, status, start, end):
    job_dict = instantiate_job(jid, status, start, end)
    rd.hmset(generate_job_key(jid), job_dict)
    return job_dict
```

## Retrieving Jobs from Redis

With our helper function, retrieving by job id is straight-forward:

```
def get_job_by_id(jid):
    return rd.hgetall(generate_job_key(jid))
```

```
Exercise. Write a function to return all jobs in the system as a list of Pytho dictionaries.
```

# The Jobs Endpoint in our Rest API

How should we design our API URLs to facilitate creating and retrieving information about jobs? To begin, jobs are
distinct nouns in our application and therefore should have their own root collection, `/jobs`.

## Creating Jobs

To create a new job, clients should:

   * Issue a POST request to the root collection (`/jobs`),
   * Input should be passed in the body of the message.
   * Message should be JSON data and the `Content-type: application/json` header and value should be set.

### Creating a Job in Flask

We'll use the usual `app.route()` to specify the URL and method, but we'll introduce a new method, `request.get_json()`
to parse the message body as json:

```
@app.route('/jobs', methods=['GET', 'POST'])
def jobs():
    """List all jobs and create new jobs."""
    if request.method == 'POST':
        try:
            job = request.get_json(force=True)
        except Exception as e:
            return True, json.dumps({'status': "Error", 'message': 'Invalid JSON: {}.'.format(e)})
        # access job inputs just like any other dictionary -- validate any required fields for type, etc.
        start = job.get('start')
        try:
            job['start'] = int(start)
        except:
            return True, json.dumps({'status': "Error", 'message': 'start parameter required and must be an integer.'})
        # fill in additional checks ...

        # once we have a valid job, we need to store it in the database:
        result = add_job(generate_jid(), 'SUBMITTED', job['start'], job['end'])
        return jsonify({'msg': 'Job created successfully.', 'result': result})

    if request.method == 'GET':
        # should return a listing of all jobs ...
```

## Retrieving Jobs By ID

Our API should be able to return the status of a single job by it's ID.

```
Exercise 1. What URL fragment and method should be used in the app.route() decorator for returning a job by an ID?

Exercise 2. Implement the endpoint to return the job associated with a given ID.
```
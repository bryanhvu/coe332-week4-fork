import json
import redis

rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=0)

def tot_primes(n):
    """ Returns the total number of primes less than or equal to `n`."""
    primes = [2]
    if n < 2:
        return 0
    for i in range(2, n+1):
        for j in range(2, i):
            if (i % j == 0):
                break
            if j == i-1:
                primes.append(i)
    return len(primes)


def store_tot_primes(max=100):
    """Sets a key in redis for each number N between 1 and `max` to the total number of primes 
    less than or equal to N.
    """
    primes = [2]
    rd.set(1, 0)
    rd.set(2, 1)
    for i in range(2, max):
        rd.set(i, tot_primes(i))

def tot_primes_cache(n):
    tot = rd.get(n)
    if tot:
        return int(tot)
    else:
        tot = tot_primes(n)
        rd.set(n, tot)
        return tot

def store_dictionary(uid, d):
    """Stores a JSON-serializable dictionary, d, in Redis under the key, uid."""
    return rd.set(uid, json.dumps(d))

def get_dictionary(uid):
    """Returns the Python dictionary associated with the key, `uid`, if one exists or `None` otherwise."""
    try:
        return json.loads(rd.get(uid))
    except:
        return None
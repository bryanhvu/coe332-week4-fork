# Motivation

Over the next several lectures, we will be extending our Flask API to enable users to perform analyses on our data set.
The basic approach will be:

  * User submits a request to a new endpoint in our API describing some analysis they want to do.
  * Since the analysis could take "a while" to execute, we will

     a) Save the request in a database and respond to the user that the analysis will eventually be run.
     b) Give the user a unique identifier with which they can check the status of their job and fetch the results when they are ready,
     c) Queue the job to run so that a worker can pick it up and run it.
     d) Build the worker to actually work the job.

We will be focusing on parts a) and b) first before moving onto parts c) and d).

## Why Use a Database?

You might be asking, why do we even need to use a database? Why not use a python object?

  * We need to save the data permanently; we want to be able to start and stop our program without losing data.
  * Eventually, we want multiple Python processes to be able to access the data at the same time.

Why not use a file?

  * It is not easy to make a file accessible to Python processes on different computers/VMs.
  * When multiple processes are reading from and writing to a file, race conditions can occur.
  * With files, are application will have to implement all the structure in the data.


# NoSQL Databases

In the previous lectures we introduced relational databases, but today we will talk about non-relational or "NoSQL"
databases. For the project, we will be using a NoSQL database called Redis, for reasons we will describe later, but
understanding SQL vs NoSQL at a high level is an important part of distributed systems engineering.

## Introduction to NoSQL

Unlike relational databases, NoSQL databases do not use tables with rows and columns connected through relations
described using relational algebra. Instead, NoSQL database servers:

  * Store data in "collections" or "logical databases".
  * Store objects in the collection instead of rows/records.
  * Can allow for missing or different attributes on objects in the same collection.
  * Objects in one collection do not relate or link to objects in another collection.

Often times, the objects themselves are JSON objects without a pre-defined schema.

### Reasons for Using SQL vs NoSQL

Both SQL and NoSQL databases have advantages and disadvantages. The primary deciding factor will be the shape of the data
(including how it might change over time) and how important the relations between data are. Specifically:

  * SQL databases allow applications to enforce relationships between types of data, including one-to-one,
    one-to-many, and many-to-many; this kind of enforcement typically must be programmed in the application when using a
    NoSQL database.
    * This enforcement feature is an advantage for some types of data - think of a bank application enforcing that there
      is exactly one owner of an account, and that the account balance is updated properly for each credit and debit.
      We need a rigid data model in this case.
    * It can be a disadvantage for other types - consider a database with a record for every tweet, including metadata
      derived from sentiment analysis: the subject(s) of the tweet and terms describing the sentiment are highly
      dependent on the tweet. For some tweets it may not be possible to determine subjects and/or sentiment. We need
      a flexible data model in this case.
  * SQL databases have challenges scaling to "large" quantities of data because of the ACID (Atomicity, Consistency,
    Isolation, Durability) guarantees they make. NoSQL databases trade ACID guarantees for "eventual consistency" and
    greater scalability. A relational database would almost certainly not scale to the "all tweets" example.

### But Why are We Using NoSQL?

While our data may look relatively small and "relational" and thus you may question our choice of NoSQL database
(it is not necessarily the choice one might make if one were building this application in the "real world"), it does
have the following advantages:

  * Quick to get started: we can get up and running with the basics of a persistent store without defining tables, schema, relations, etc.
  * Muli-use: Our choice of NoSQL db will double as a persistent message queue later.


## Introduction to Redis

Redis is a very popular NoSQL database and "data structure store" with lots of advanced features. We will use Redis for
our class project.

### Key-Value Store

The items stores in Redis are structured as Key:Value objects.

  * The primary requirement is that the key be unique within a "database".
  * A single Redis server can support multiple databases, indexed by an integer (e.g., `db=0`, `db=1`, etc.)

Some notes on keys:

  * Keys are often strings but can be any "binary sequence"; e.g., the contents of a JPEG file can be used as a key.
  * Having long keys, e.g., over 1000 bytes long, can lead to performance issues.
  * Using strings for keys and structuring the key based on some format is often the best choice; e.g., <object_type>:<object_id>

Some notes on values:

  * Values are typed; some of the primary types include:
    * Binary-safe strings
    * Lists: sorted collections of strings
    * Sets: unsorted, unique collections of strings
    * Hashes: maps of fields with associated values; both field and value are type string.
  * There is no native "JSON" type; to store JSON, one can use an encoding and store the data as a binary-safe string, or one
    can use a hash and convert the object into and out of JSON.


### More on Basic String Type Values

Keep in mind that the basic string type is a "binary-safe" string; this means it must include an encoding.

  * In Python terms, the string is stored and returned as type "bytes".
  * By default, the string will be encoded with UTF-8, but we can specify the encoding when storing the string.
  * Since bytes are returned, it will be our responsibility to decode using the same encoding.

### Hash Maps

Hashes provide another way of storing dictionary-like data in Redis. The values of the keys are string types. We will
see more on hashes in the Working with Redis in Python section below.


### Runing Redis

Now that we understand Docker, starting and running a Redis server can be accomplished in two easy steps.
Assuming docker is running:

  * Pull the official Redis image for version 5.0.0: `$ docker pull redis:5.0.0`
  * Start the redis server in a container: `$ docker run -p 6379:6379 redis:5.0.0`

Note: the above command will start the Redis container in the foreground which can be helpful for seeing logs, etc.
However, you will have the need to start it in the background. To do so, pass the `-d` flag to `docker run`.

```
Exercise. SSH to your VM and start up a Redis server running in a docker container.
```

## Working with Redis in Python

### Redis Library

To work with redis in Python, we will use the Redis Python library, `redis`. It can be installed with

```
$ pip install redis
```

### First Steps

To get started, we first import the redis library

```
>>> import redis
```

We next create a client connection to the Redis server and specify a database using the `StrictRedis` class:

```
>>> rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=0)
```

Some notes:

  * We are using the IP of the gateway (`172.17.0.1`) on the Docker0 interface (bridge) and the standard
Redis port `6379`). This works because we mapped the container port to the same port on the bridge.
  * As noted above, Redis organizes collections into "databases" identified by an integer index. Here, we are
    specifying `db=0`; if that database does not exist it will be created for us.


### Creating, Updating and Retrieving Objects from Redis in Python

With our Redis client, `rd`, lets create and save our first Redis object. When working with the basic string type,
we will use the `get` and `set` methods.

#### The `.set()` and `.get()` Methods

We can create and/or update a key-value string pair using `rd.set(key, value)`. For example:

```
>>> rd.set('my_key', 'my_value')
Out[1]: True
```

This saved a key in the Redis server (`db=0`) with key `my_key` and value `my_value`. Note the method returned `True`,
indicating that the request was successful.

We can retrieve it with:

```
>>> rd.get('my_key')
Out[2]: b'my_value'
```

Note that `b'my_value'` was returned; in particular, Redis returned *binary data* (i.e., type `bytes`). In other words,
the string was encoded for us (in this case, using Unicode). We could have been explicit and set the encoding ourselves.

Redis is often used as a "caching" database. The following exercises illustrate the basic idea.

```
Exercise 1. The following function returns the total number of primes less than or equal to its input.

def tot_primes(n):
    """ Returns the total number of primes less than or equal to `n`."""
    primes = [2]
    if n < 2:
        return 0
    for i in range(2, n+1):
        for j in range(2, i):
            if (i % j == 0):
                break
            if j == i-1:
                primes.append(i)
    return len(primes)


Using a `for` loop and the function above, write a function to save a Redis object for each number `N` between 1 and
an input `max` (default is 100)`. The key should be the number `N` and the value should be the calculated number of
primes between 1 and `N`.

def store_tot_primes_redis(max=100):
    """ Your code here """
    # . . .

Exercise 2. Call your store_tot_primes() function and validate using redis `get()`s that the objects were stored.

Exercise 3. Try calling store_tot_primes(5000). How long does it take? How about 10000?

Exercise 3 (memoization). Write a function, tot_primes_cache(n), that returns the total number of primes less than
or equal to the integer `n` using the following algorithm: first, check to see if the result has already been stored in
Redis under the appropriate key, and if it has, return that immediately. Otherwise, calculate it using the `tot_primes()`
function and store it in redis before returning it.
```


### Storing and Retrieving JSON

The binary-safe string data type used in Redis means a little care is required for storing pure JSON. Consider the
following example:

```
>>> import json
>>> d = {'a': 1, 'b': 2}
>>> rd.set('k1', json.dumps(d))
>>> json.loads(rd.get('k1'))
```

Whether this works or not depends on your version of Python. Specifically, in versions < Python 3.6, the above will
break, as the encoding has not been made explicit. In Python 3.6, the `json` library was updated to automatically detect
UTF-8, UTF-16 and UTF-32 encoded data (cf., https://bugs.python.org/issue10976).

To get this working in earlier versions of Python, specify the encoding:

```
>>> json.loads(rd.get('k1').decode('utf-8'))
Out[1]: {'a': 1, 'b': 2}
```

### Hashes

Hashes provide another way of storing dictionary-like data in Redis.

 * Hashes are useful when different fields are encoded in different ways; for example, a mix of binary and unicode data.
 * Each field in a hash can be treated with a separate decoding scheme, or not decoded at all.
 * Use `hset` to set a single field value in a hash; use `hmset` to set multiple fields at once.
 * Similarly, use `hget` to get a single field within a hash, use `hmget` to get multiple fields, or use `hgetall` to get
   all of the fields.

THe following examples will make things clear:

```
>>> rd.hmset('k2', {'name': 'Joe', 'email': 'jstubbs@tacc.utexas.edu'})
>>> rd.hgetall('k2')
Out[1]: {b'name': b'Joe', b'email': b'jstubbs@tacc.utexas.edu'}
>>> rd.hset('k2', 'name', 'Joe Stubbs')
>>> rd.hgetall('k2')
Out[2]: {b'name': b'Joe Stubbs', b'email': b'jstubbs@tacc.utexas.edu'}
>>> rd.hget('k2', 'name')
Out[3]: b'Joe Stubbs'
>>> rd.hmget('k2', 'name', 'email')
Out[4]: [b'Joe Stubbs', b'jstubbs@tacc.utexas.edu']
```

```
Exercise 1. Write a Pyton function, `store_dictionary(uid, d)` that accepts a key, `uid`, and JSON-serializable
dictionary, `d`, and stores a Redis object with key `uid` and value the serialized `d`.

Exercise 2. Write a Python function, `get_dictionary(uid)` that accepts a key, `uid`, and returns a Python dictionary
associated with that key if one exists or `None` otherwise.
```
# Miscellanea

## git

* do you git check-in from your private vms?
* working from multiple places 
  * git merge = combine changes from a different source
  * git fetch = retrieve changes from remote, keep in background
  * git pull = fetch and merge)
* 

# Docker review

* Build the flask image
* Make small change to Dockerfile then rebuild (note it does not re-run all steps)

# Run Flask in Docker (using TACC demo)

The goal is to incorporate repeatable deployment with Docker containers into your workflow.

Try this:

    cd example_flask
    docker build -t flask-tutorial .
    ...
    docker run -it -p 5000:5000 flask-tutorial
    
From another window:

    curl localhost:5000
    Hey, we have Flask in a Docker container!

# Run Flask in Docker ourselves

* Get a dataset
* Build a Dockerfile
* Create REST API for your data
* Run container 
* Hit with curl
* Mount dataset into container at runtime vs. include in image
* docker-compose

# Other tools

* docker hub (upload base images)
* See/compare example_flask/Dockerfile.headstart
* nginx
* uwsgi

# Discuss 

* What is saved locally and what is saved outside of the VM?
* What are the advantages/disadvantages of having an image at docker hub vs. having the Dockerfile in git/bitbucket?
* Check out storage space used by images
  * docker images
  * df -h

## Links

* A more complex flask/nginx/uwsgi setup: https://github.com/TACC/irksome-guacamole
* Data set from https://www.ncdc.noaa.gov/cag/global/time-series/globe/land_ocean/ytd/12/1880-2016.json

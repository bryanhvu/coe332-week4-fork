# Midterm Practice Exam

**Note that the midterm will be a written-on-paper exam.** 

* What is source control (e.g. git) and why is it important?

* What is JSON is and why is it is useful?

* Containers or VMs?
    * Virtualizes a server - often includes a package manager, init system, hundreds of simultaneous processes running.
    * Virtualizes a process/application: 1 primary PID.
    * Can run dozens on a single server.
    * Can run 1,000+ on a single server

* Given the JSON below, write code to print the year of each prize.

* Given the JSON below, write code to print the total number of winners for each gender.

* Given the JSON below, write code to print each winner�s Surname, First name, and category of prize.

Use this data for the above 3 questions::

```
{
    "laureates": [
        {
            "firstname": "Frances H.",
            "surname": "Arnold",
            "gender": "female",
            "prizes": [
                {
                    "category": "chemistry",
                    "year": "2018"
                }
            ]
        },
        {
            "firstname": "George P.",
            "surname": "Smith",
            "gender": "male",
            "prizes": [
                {
                    "category": "chemistry",
                    "year": "2018"
                }
            ]
        },
        {
            "firstname": "Sir Gregory P.",
            "surname": "Winter",
            "gender": "male",
            "prizes": [
                {
                    "category": "chemistry",
                    "year": "2018"
                }
            ]
        }
    ]
}
```





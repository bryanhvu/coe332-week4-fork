# flask_web/app.py

from flask import Flask,jsonify,json
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hey, we have Flask in a Docker container!'

@app.route('/anomalies')
def getanomalies():
    with open('anomalies.json', 'r') as f:
        anomalies = json.load(f)
    return jsonify(anomalies)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')


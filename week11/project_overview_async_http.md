# Putting our pieces together - Demos and Question Time!!

We're going to pull down and deploy our API step by step on our new VM!

* Create your new VM (if you haven't already)
* ssh in (let us know if you need help)
* clone down your project
* run your docker commands in the folder where your dockerfile is:
  * `docker build -t <your-image-tag> .`
  * `docker-compose up`

Did it work? 

# Asynchronous HTTP Requests

Asynchronos HTTP requests are what we're attempting to implement with our jobs endpoint and worker. Sometimes an API needs to do work that cannot be returned immediately, 
such as our graph building functionality. This is a very small example, but large visualizations and data-crunching can often take hours or even days. More complex systems can even provide the ability
to notify users when their jobs are done. In this case, we would 
need to implement an endpoint that kicks off this week at some point, and allows the user to query the status, and eventually get the result. 

Let's look through our worker, and watch it in action!


# Questions? Concerns? 

Feel free to take the rest of the time to work on your project and ask questions if you have any!

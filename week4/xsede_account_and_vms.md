# XSEDE Accounts and VM access

## XSEDE 

XSEDE is a nationwide partnership that supports open research. Several of our resources at TACC are partially allocated by XSEDE.

Please do before next class period:

* Visit https://portal.xsede.org/#/guest
* Click "Create Account"
* Fill out forms. Use your UT email address. 
* *Take note* of registration key... you will need it later
* Try to request the same username as your TACC username

## VMs

* VMs are semi-disposable (i.e. crops vs houseplants). 
* Make a habit of checking in your work to git regularly. 
* That way if something happens to the VM, your work is safe.
* Also back up everything else you might need to run your code (e.g. 
    * system libraries
    * sensitive variables/passwords
    * binary data that does not live in git 
    * manual steps not captured in code
* Think about everything you'd need to reproduce your environment. One of the challenges of development vs production is making sure someone else can reproduce your work. Laptop vs. VM in the cloud.
* In Bitbucket/Github, these README.md files are nice.
* In a production environment, using VMs means backing up application data, configuration, and keeping track of application state.
* Try now from coe332/isp node (find your ip below): 

    ssh ubuntu@<your_vm_ip>

You may have to answer yes to this question:

    The authenticity of host '129.114.16.93 (129.114.16.93)' can't be established.
    ECDSA key fingerprint is SHA256:xKXfc2iQMhgYoJTyh1ZqmgafMjOcEYEaiU0djmLyFrs.
    ECDSA key fingerprint is MD5:88:82:61:e2:7c:c7:11:41:c0:8e:bb:7c:ee:71:60:0d.
    Are you sure you want to continue connecting (yes/no)? 

### VM IP List:

    * ajpye-1: 129.114.17.171
    * ashok-1: 129.114.16.52
    * bbridges-1: 129.114.104.55
    * bismuth-1: 129.114.104.25
    * cgarcia-1: 129.114.17.97
    * charlie-1: 129.114.17.119
    * cja2345-1: 129.114.16.11
    * clewis19-1: 129.114.104.189
    * cmarnold-1: 129.114.17.167
    * em27799-1: 129.114.16.67
    * hammock-1: 129.114.17.158
    * jakejan-1: 129.114.16.7
    * jdc5646-1: 129.114.17.235
    * jhsansom-1: 129.114.16.36
    * jlooney-1: 129.114.16.13
    * jstubbs-1: 129.114.16.83
    * kreshel-1: 129.114.104.112
    * lherrera-1: 129.114.17.252
    * matt_g-1: 129.114.17.138
    * mattscar-1: 129.114.17.150
    * mdl2665-1: 129.114.104.231
    * mpackard-1: 129.114.16.93
    * nboeker-1: 129.114.104.142
    * paigew-1: 129.114.16.9
    * yyd67-1: 129.114.17.215


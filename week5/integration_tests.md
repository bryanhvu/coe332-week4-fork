# Integration Tests, Part 1

Unlike unit tests, integration tests exercise multiple components, functions or units of a software system at once.
Recall some previous remarks we made about integration tests:

  * Each test targets a higher-level capability, requirement or behavior of the system, and exercises multiple components of the system working together.
  * Broader scope means fewer tests are required to cover the entire application/system.
  * A given test failure provides more limited information as to the root cause.

It's worth pointing out that our definition of integration test leaves some ambiguity. You will also see the term "functional tests"
used for tests the exercise entire aspects of a software system.

## Challenges When Writing Integration Tests

Integration tests against large, distributed systems with lots of components that interact face some challenges.

  * We want to keep tests independent so that a single test can be run without its result depending on other tests.
  * Most interesting applications change "state" in some way over time; e.g., files are saved/updated, database records are written, queue systems updated. In order to properly test the system, specific state must be established before and after a test (for example, inserting a record into a database before testing the "update" function).
  * Some components interact have external interactions, such as an email server, a component that makes an update in an external system (e.g. github) etc. A decision has to be made about whether or not this functionality will be validated in the test and if so, how.

Over the course of the semester, we will introduce some techniques for dealing with some of these challenges. For now though,
our API doesn't actually change state or have any external components that it interacts with, so things are pretty simple.

## Big picture: Build Servers and Continuous Integration

A "build server" or "integration serer" is a dedicated server (or VM) that prepares software for release. The server automates common tasks, including

  * Building software binaries from source code (for compiled languages)
  * Running tests
  * Creating images, installers or other artifacts.
  * Deploying/installing the software onto

We are ultimately aiming for the following "Continuous Integration" work flow or process; this mirrors the process used by a number of teams working on "large" software systems, both in academia and industry:

  * Developers (i.e., you) check out code onto a machine where they will do their work. This could be a VM somewhere or their local laptop.
  * They make changes to the code to add a feature or fix a bug.
  * Once their work is done they add any additional tests as needed and then run all unit tests "locally" (i.e., on the same machine).
  * Assuming the tests pass, the develop commits their changes and pushes to the remote origin (in this case, bitbucket).
  * A pre-established build server gets a message from the origin that a new commit was pushed.
  * The build server:
    * Checks out the latest version
    * Executes any build steps to create the software. (We won't have this step yet)
    * Runs unit tests
    * Starts an instance of the system
    * Runs integration tests
    * Deploys the software to a staging environment

If any one of the steps above fails, the process stops. In such a situation, the code defect should be addressed as soon as possible.

### Enabling Large Teams to Work on a System Simultaneously

The primary goal of Continuous Integration (CI) is to enable multiple developers to work on the same code base while
ensuring the quality of the final product.

This involves the following challenges:

  * No one person has complete knowledge of the entire system.
  * Multiple changes can be happening at the same time. Even if the changes are made in different components, it is possible for something to break when they are integrated.


## Initial Integration Tests for Our Flask API

For our first set of integration tests, we'll use the following strategy:

  * Start the flask API in one session.
  * Use `py.test` and `requests` to make requests directly to the running API server.
  * check various aspects of the response; each check can be done with a simple `assert` statement, just like for unit tests.

Exercise. Create a new file, test_api.py, and write a single test function that uses the requests library to make a GET request to the /<root_collection> endpoint and check the response for:

  * The response returns a 200 status code.
  * The response returns a valid JSON string.
  * The response can be decoded to a Python list.
  * Each element of the decoded list is a Python dictionary.
  * Each dictionary in the result has three keys.
  * Verify that the type of each key’s value is correct.
  * Check that the number of dictionaries returned is correct.
  * Check that values of the first and last dictionaries in the list have correct values.

Make sure your flask API is running, and use the py.test program to run your tests. Validate the results.



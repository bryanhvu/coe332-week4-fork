# Sign up for XSEDE account

Please visit this page and fill out the form: [Create an XSEDE User Portal account](https://portal.xsede.org/my-xsede?p_p_id=58&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&_58_struts_action=%2Flogin%2Fcreate_account) 

Make sure to follow the instructions to verify the account too.

We will use this to create our own virtual machines.


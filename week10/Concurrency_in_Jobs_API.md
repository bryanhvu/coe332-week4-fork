## Daemonizing the Worker

Let's extend our docker-compose file so that we have everything we need to start up our basic system from last time.

Recall that when creating a Docker image both an `entrypoint` and a `command` can be defined. Together, these
attributes determine which process is executed inside containers that are started from the image.

The entrypoint and command behave as follows:

  * When specified within the image, these are the default values for a container.
  * The full process that is executed is the concatenation of the entrypoint and the command.
  * A different command can be supplied at run time for a specific container by passing a string after the image name;
  e.g., `docker run <my_image> <my_command>`
    In this case, `<my_command>` is appended to the entrypoint supplied in the image.
  * A different entrypoint can be supplied by using the `--entrypoint` flag before the image.
  e.g., `docker run --entrypoint=<my_entry_point> <my_image>

Some common entrypoints and there corresponding commands:

  * Entrypoint: `python`, run the Python interpreter; commands: `/path/to/app.py`.
  * Entrypoint: `bash` (or `/bin/bash`), run a bash shell. commands - not used.
  * Entrypoint: `redis`, run the redis server; commands: arguments to the Redis server.

```
Exercise 1. Add an entry to the docker-compose file for Redis,
if not there already. Make sure to map the standard Redis port
(6379) so that the other components of your system can interact
with it.
```

```
Exercise 2. Add an entry to your compose file to start one
worker container. The image can be the same as that used
by the Fask API server, but the entrypoint and/or command
will need to change.
```

